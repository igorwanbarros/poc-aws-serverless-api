SHELL=/bin/bash

RED     := \033[0;31m
GREEN   := \033[0;32m
BLUE    := \033[0;34m
RESET   := \033[0m

INFO := ${GREEN}[INFO]:${RESET}

cli:
	@printf "${INFO} loading dynamodb...\n" \
	&& docker-compose up -d poc-dynamodb \
    && printf "\n\n${INFO} creating tables" \
    && npm run table:init \
    && printf "\n\n${INFO} runing tests" \
    && npm run test:watch
