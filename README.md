# POC AWS API Serverless

## Step by step to install

### Config serverless credentials

```shell
    serverless config credentials -o --provider aws --key={your-key} --secret={your-secret}
```

### Configura AWS credentials

```shell
    aws configure
```

Inform your AWS Access Key ID and AWS Secret Key ID

### Install dynamo locally

```shell
    sls dynamodb install
    // or serverless dynamodb install
```

### Run locally

```shell
    npm start
```

### Deploy

```shell
    sls deploy
    // or serverless deploy
```
