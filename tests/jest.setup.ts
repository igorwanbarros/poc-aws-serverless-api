import { ManagerDatabase } from "./database/setup/database-manager";
import { PostSchema } from "./database/setup/tables";

const managerDababase = new ManagerDatabase;

beforeAll(async () => {
    jest.useRealTimers();
    await managerDababase.createTable(PostSchema);
});

afterAll(async () => {
    await managerDababase.deleteTable({ TableName: PostSchema.TableName });
});
