import * as uuid from "uuid";
import Post from "@domain/models/post";
import { PostRepository } from "@infra/database";
import { IRepository } from "@infra/database/contracts";

const mockPostRepository: IRepository<Post> = {
  create: jest.fn(async (post: Partial<Post>): Promise<Post> => {
    return {
      id: uuid.v4(),
      created_at: "2023-01-01 08:00:00",
      ...post,
    } as Post;
  }),
  update: jest.fn(async (id: string, item: Partial<Post>): Promise<Post> => {
    return {
      id,
      updated_at: "2023-01-01 08:00:00",
      ...item,
    } as Post;
  }),
  delete: jest.fn(async (_: string): Promise<boolean> => {
    return true;
  }),
  first: jest.fn(async (_id: string): Promise<Post> => {
    return {} as Post
  }),
  all: jest.fn(async (): Promise<Post[]> => { return []; }),
}

export default mockPostRepository as PostRepository
