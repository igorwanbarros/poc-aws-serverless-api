import "reflect-metadata";
import { PostService } from "@domain/services";

import { PostController } from "@domain/controllers/post-controller";
import Post from "@domain/models/post";
import { DynamoClient } from "@infra/database/dynamo-client";
import { PostRepository } from "@infra/database";


const variablesInit = () => {
  const dbClient = new DynamoClient();
  const repository = new PostRepository(dbClient, process.env.POSTS_TABLE);
  const service = new PostService(repository);
  const controller = new PostController(service);

  return {
    controller,
    repository,
    service
  }
}

describe("PostController.ts", () => {
  it("test create post", async () => {
    const { controller } = variablesInit()
    const result = await controller.create({
      body: { title: "banana", description: "laranja" },
      headers: {},
    });

    expect(result).toHaveProperty("statusCode", 200);
    expect(result).toHaveProperty("body");
    expect(result.body).toEqual(expect.stringContaining("banana"));
  });

  it("test create post throws exception", async () => {
    const { controller, repository } = variablesInit()
    repository.create = jest.fn(async (): Promise<Post> => {
      throw new Error();
    });

    const result = await controller.create({
      body: { title: "banana", description: "laranja" },
      headers: {},
    });

    expect(result).toHaveProperty("statusCode", 500);
    expect(result).toHaveProperty("body");
  });

  it("test update post", async () => {
    const { controller, service } = variablesInit()

    const post = await service.create({
      title: "Post Created",
      description: "description ...",
    } as Post);

    const result = await controller.update({
      pathParameters: { id: post.id },
      body: {
        title: "Updated Post",
        description: "description my post",
        active: true,
      },
      headers: {},
    });

    expect(result).toHaveProperty("statusCode", 200);
    expect(result).toHaveProperty("body");
    expect(result.body).toEqual(expect.stringContaining("Updated Post"));
  });

  it("test update post not found", async () => {
    const { controller, service } = variablesInit();

    await service.create({title: "One Post"} as Post);
    const deletedPost = await service.create({title: "Two Post"} as Post);
    await service.delete(deletedPost.id);

    const result = await controller.update({
      pathParameters: { id: deletedPost.id },
      body: { title: "Updated Post" },
      headers: {},
    });

    expect(result).toHaveProperty("statusCode", 404);
    expect(result).toHaveProperty("body");
    expect(result.body).toEqual(expect.stringContaining("resource not found."));
  });

  it("test update post throw error", async () => {
    const { controller, repository, service } = variablesInit();

    const post = await service.create({ title: 'Post One' } as Post);

    repository.update = jest.fn(async (): Promise<Post> => {
      throw new Error();
    });

    const result = await controller.update({
      pathParameters: { id: post.id },
      body: { title: "Updated Post" },
      headers: {},
    });

    expect(result).toHaveProperty("statusCode", 500);
    expect(result).toHaveProperty("body");
  });

  it("test get posts", async () => {
    const { controller, service } = variablesInit();

    await service.create({ title: 'Post One' } as Post);
    await service.create({ title: 'Post Two' } as Post);

    const result = await controller.get({
      body: {},
      pathParameters: null,
      headers: {},
    });

    expect(result).toHaveProperty("statusCode", 200);
    expect(result).toHaveProperty("body");
    expect(result.body).toEqual(expect.stringContaining("Post One"));
    expect(result.body).toEqual(expect.stringContaining("Post Two"));
  });

  it("test get posts throws exception", async () => {
    const { controller, repository } =  variablesInit();

    repository.all = jest.fn(async (): Promise<Post[]> => {
      throw new Error
    });

    const result = await controller.get({
      body: {},
      pathParameters: null,
      headers: {},
    });

    expect(result).toHaveProperty("statusCode", 500);
    expect(result).toHaveProperty("body");
  });

  it("test first posts", async () => {
    const { controller, service } = variablesInit();

    await service.create({title: "One Post"} as Post);
    const post = await service.create({title: "Two Post"} as Post);

    const result = await controller.get({
      body: {},
      pathParameters: { id: post.id },
      headers: {},
    });

    expect(result).toHaveProperty("statusCode", 200);
    expect(result).toHaveProperty("body");
    expect(result.body).toEqual(expect.stringContaining("Two Post"));
  });

  it("test first posts not found", async () => {
    const { controller, service } = variablesInit();

    await service.create({title: "One Post"} as Post);
    const deletedPost = await service.create({title: "Two Post"} as Post);
    await service.delete(deletedPost.id);

    const result = await controller.get({
      body: {},
      pathParameters: { id: deletedPost.id },
      headers: {},
    });

    expect(result).toHaveProperty("statusCode", 404);
    expect(result).toHaveProperty("body");
  });

  it("test delete post", async () => {
    const { controller, service } = variablesInit();

    const postDeleted = await service.create({title: "One Post"} as Post);

    const result = await controller.delete({
      pathParameters: { id: postDeleted.id },
      body: {},
      headers: {},
    });

    expect(result).toHaveProperty("statusCode", 200);
    expect(result).toHaveProperty("body");
    expect(result.body).toEqual(
      expect.stringContaining("resource deleted successfully")
    );
  });

  it("test delete post not found", async () => {
    const { controller, service } = variablesInit();

    await service.create({title: "One Post"} as Post);

    const deletedPost = await service.create({title: "Two Post"} as Post);
    await service.delete(deletedPost.id);

    const result = await controller.delete({
      pathParameters: { id: deletedPost.id },
      body: {},
      headers: {},
    });

    expect(result).toHaveProperty("statusCode", 404);
    expect(result).toHaveProperty("body");
    expect(result.body).toEqual(expect.stringContaining("resource not found."));
  });

  it("test delete post throws exception", async () => {
    const { controller, service, repository } = variablesInit();

    await service.create({title: "One Post"} as Post);
    const deletedPost = await service.create({title: "Two Post"} as Post);

    repository.delete = jest.fn(async (): Promise<boolean> => {
      throw new Error;
    });

    const result = await controller.delete({
      pathParameters: { id: deletedPost.id },
      body: {},
      headers: {},
    });

    expect(result).toHaveProperty("statusCode", 500);
    expect(result).toHaveProperty("body");
  });
});
