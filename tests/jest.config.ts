import { resolve } from 'path'
import { pathsToModuleNameMapper } from 'ts-jest'

import { compilerOptions } from '../tsconfig.paths.json'

const root = resolve(__dirname, '..')

export default {
  rootDir: root,
  displayName: 'end2end',
  setupFilesAfterEnv: ['<rootDir>/tests/jest.setup.ts'],
  testMatch: ['<rootDir>/tests/**/*.test.ts'],

  clearMocks: true,
  collectCoverage: true,
  coverageDirectory: 'coverage',
  coverageProvider: 'v8',
  preset: 'ts-jest',
  testEnvironment: 'jest-environment-node',
  transform: {
    '^.+\\.ts?$': 'ts-jest'
  },
  moduleNameMapper: pathsToModuleNameMapper(compilerOptions.paths, {
    prefix: '<rootDir>/'
  })
}
