import { CreateTableInput } from "aws-sdk/clients/dynamodb";

export const PostSchema: CreateTableInput = {
    TableName: "Posts-stage",
    KeySchema: [{ AttributeName: "id", KeyType: "HASH" }],
    AttributeDefinitions: [{ AttributeName: "id", AttributeType: "S" }],
    ProvisionedThroughput: {
      ReadCapacityUnits: 5,
      WriteCapacityUnits: 5,
    }
}
