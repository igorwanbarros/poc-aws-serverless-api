import { ManagerDatabase } from "./database-manager";
import { PostSchema } from "./tables";

try {
  const manager = new ManagerDatabase()

  manager.createTable(PostSchema).then(console.log)
} catch (error) {
  console.error(error);
}
