import * as AWS from "aws-sdk";
import { CreateTableInput, DeleteTableInput } from "aws-sdk/clients/dynamodb";

export class ManagerDatabase {
  private dynamodb: AWS.DynamoDB;

  public constructor() {
    this.dynamodb = new AWS.DynamoDB({
      region: "us-west-2",
      endpoint: "http://localhost:4566",
    });
  }

  public async createTable(tableInput: CreateTableInput): Promise<string> {
    const listTables = await this.dynamodb.listTables().promise();
    const exists: boolean =
      listTables.TableNames.filter((table) => table === tableInput.TableName)
        .length > 0;

    if (exists) {
      return `Table ${tableInput.TableName} already exists.`;
    }
    
    await this.dynamodb.createTable(tableInput).promise();
    return `Table ${tableInput.TableName} created.`;
  }

  public async deleteTable(tableInput: DeleteTableInput): Promise<string> {
    const listTables = await this.dynamodb.listTables().promise();
    const exists: boolean =
      listTables.TableNames.filter((table) => table === tableInput.TableName)
        .length > 0;

    if (!exists) {
      return `Table ${tableInput.TableName} not exists.`;
    }
    
    await this.dynamodb.deleteTable(tableInput).promise();
    return `Table ${tableInput.TableName} deleted.`;
  }
}
