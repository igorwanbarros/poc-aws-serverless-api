import { StatusCode } from "@domain/contracts"

/**
 * @deprecated
 */
export const responseJson = (status: StatusCode, data: any): any => {
  const response = Array.isArray(data)
    ? { status, data }
    : (data ?? { status })

  return {
    statusCode: status,
    body: JSON.stringify(response)
  }
}
