import middy from "@middy/core";

export const errorMiddeware = (request: middy.Request) => {
    const response = request.response ?? {};
    const error = <any>request.error;

    if (response.statusCode != 400) return;
    if (!error.expose || !error.cause) return;

    response.headers["Content-Type"] = "application/json";

    const causes = error.cause.map((cause) => {
      return {
        message: cause.message,
        validate: cause.params
      }
    })

    response.body = JSON.stringify(causes);

    return response
  }
