import { Handler } from "aws-lambda";
import middy from "@middy/core";
import cors from "@middy/http-cors";
import httpErrorHandler from "@middy/http-error-handler";
import middyJsonBodyParser from "@middy/http-json-body-parser";
import validator from "@middy/validator";
import { transpileSchema } from "@middy/validator/transpile";
import { errorMiddeware } from "@infra/http/middlewares/error";


const middify = (handler: Handler, schema = {}) => {
  return middy(handler)
    .use(middyJsonBodyParser())
    .use(cors())
    .use({ onError: errorMiddeware })
    .use(
      validator({
        eventSchema: transpileSchema(schema),
      })
    )
    .use(httpErrorHandler())
};

export default middify;
