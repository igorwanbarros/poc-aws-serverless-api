import { ScanInput, ScanOutput } from "aws-sdk/clients/dynamodb";

export interface IDynamoClient {

  select(tableName: string, filter: Omit<ScanInput, "TableName">): Promise<ScanOutput>;

  insert<T>(tableName: string, item: Partial<T>): Promise<boolean>;

  update<T>(
    tableName: string,
    primaryKey: string,
    item: Partial<T>
  ): Promise<T>;

  delete(tableName: string, primaryKey: string): Promise<boolean>;
}

export interface IRepository<T> {
  create(item: T): Promise<T>;

  update(id: string, item: Partial<T>): Promise<T>;

  delete(id: string): Promise<boolean>;

  first(id: string): Promise<T>;

  all(filter:any): Promise<T[]>;
}
