import {
  DocumentClient,
  ScanInput,
  ScanOutput,
  PutItemInput
} from "aws-sdk/clients/dynamodb";
import { IDynamoClient } from "@infra/database/contracts";

export class DynamoClient implements IDynamoClient {
  private readonly client: DocumentClient;

  public constructor() {
    this.client = this.createClient();
  }

  public async select(
    tableName: string,
    filter: Omit<ScanInput, "TableName">
  ): Promise<ScanOutput> {
    const params: ScanInput = {
      ...filter,
      TableName: tableName,
    };

    const result = await this.client.scan(params).promise();

    return result;
  }

  public async insert<T>(
    tableName: string,
    item: Partial<T>
  ): Promise<boolean> {
    const params: PutItemInput = {
      TableName: tableName,
      Item: item
    };

    const result = await this.client.put(params).promise();

    return result.$response.httpResponse.statusCode === 200;
  }

  public async update<T>(
    tableName: string,
    primaryKey: string,
    item: Partial<T>
  ): Promise<T> {
    const attributes = item
    delete attributes["id"]

    const params = {
      TableName: tableName,
      Key: { id: primaryKey },
      UpdateExpression: `set ${Object.entries(attributes)
        .map(([key]) => `#${key} = :${key}, `)
        .reduce((acc, str) => acc + str)
        .slice(0, -2)}`,
      ExpressionAttributeNames: Object.keys(attributes).reduce(
        (acc, key) => ({
          ...acc,
          [`#${key}`]: key,
        }),
        {}
      ),
      ExpressionAttributeValues: Object.entries(attributes).reduce(
        (acc, [key, value]) => ({
          ...acc,
          [`:${key}`]: value,
        }),
        {}
      ),
      ReturnValues: "ALL_NEW",
    };

    const result = await this.client.update(params).promise();

    return result.Attributes as T;
  }

  public async delete(tableName: string, primaryKey: string): Promise<boolean> {
    const params = {
      TableName: tableName,
      Key:{ id: primaryKey }
    };

    const result = await this.client.delete(params).promise();

    return result.$response.httpResponse.statusCode === 200;
  }

  private createClient(): DocumentClient {
    if (process.env.STAGE) {
      return new DocumentClient({
        region: "us-west-2",
        endpoint: "http://localhost:4566",
      });
    }

    return new DocumentClient();
  }
}
