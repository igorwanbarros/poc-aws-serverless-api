import Post from "@domain/models/post";
import { inject, injectable } from "tsyringe";
import { IDynamoClient, IRepository } from "../contracts";

@injectable()
class PostRepository implements IRepository<Post> {
  constructor(
    @inject("IDynamoClient")
    private readonly dynamoClient: IDynamoClient,
    @inject("POST_TABLE_NAME")
    private readonly tableName: string
  ) {}

  public async create(item: Post): Promise<Post> {
    const created = await this.dynamoClient.insert(this.tableName, item)

    if (created) {
      return item;
    }

    throw new Error("Item not Created!")
  }

  public async update(id: string, item: Partial<Post>): Promise<Post> {
    return await this.dynamoClient.update(this.tableName, id, item)
  }

  public async delete(id: string): Promise<boolean> {
    return await this.dynamoClient.delete(this.tableName, id)
  }

  public async first(id: string): Promise<Post> {
    const result = await this.dynamoClient.select(this.tableName, {
      FilterExpression: "#id = :id",
      ExpressionAttributeNames: {
        "#id": "id"
      },
      ExpressionAttributeValues: {
        ":id": id as unknown
      }
    })

    if (result.Items) {
      return result.Items.at(0) as unknown as Post
    }

    throw new Error("Item not found.");
  }

  public async all(filter?: any): Promise<Post[]> {
    const scan = await this.dynamoClient.select(this.tableName, filter ?? {})

    return scan.Items as unknown as Post[]
  }
}

export default PostRepository
