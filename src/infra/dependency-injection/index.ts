import "reflect-metadata";
import { container } from "tsyringe";

import { PostRepository } from "@infra/database";
import { DynamoClient } from "@infra/database/dynamo-client";

import { PostService } from "@domain/services";
import { PostController } from "@domain/controllers/post-controller";

container.registerSingleton("IDynamoClient", DynamoClient);

container.registerInstance("POST_TABLE_NAME", process.env.POSTS_TABLE);

container.register("PostRepository", PostRepository);
container.register("PostService", PostService);
container.register("PostController", PostController);

export const bootstrap = container;
