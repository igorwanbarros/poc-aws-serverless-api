import { Response, StatusCode } from "@domain/contracts";

export class BaseController {
  protected response(status: number, data?: any): Response {
    const isArray = Array.isArray(data)
    let response = isArray
      ? { status, data }
      : data;

      if (status === StatusCode.NOT_FOUND) {
        response = {
          message: "resource not found.",
          status,
          ...response
        }
      }

    return {
      statusCode: status,
      body: JSON.stringify(response),
    };
  }
}
