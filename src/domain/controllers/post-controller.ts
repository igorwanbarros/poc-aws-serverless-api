import { Request, StatusCode } from "@domain/contracts";
import Post from "@domain/models/post";
import { PostService } from "@domain/services";
import { inject, injectable } from "tsyringe";
import { BaseController } from "./base-controller";

@injectable()
export class PostController extends BaseController {
  public constructor(
    @inject("PostService")
    private readonly servicePost: PostService
  ) {
    super();
  }

  public async create(request: Request) {
    try {
      const post = await this.servicePost.create(request.body as Post);

      return this.response(StatusCode.SUCCESS, post);
    } catch (error) {
      return this.response(StatusCode.ERROR, error);
    }
  }

  public async update(request: Request) {
    const { id } = request.pathParameters;
    const { body } = request;

    try {
      const old = await this.servicePost.get(id);

      if (!old) {
        return this.response(StatusCode.NOT_FOUND);
      }

      const post = await this.servicePost.update(id, body as Post);

      return this.response(StatusCode.SUCCESS, post);
    } catch (error) {
      return this.response(StatusCode.ERROR, error);
    }
  }

  public async delete(request: Request) {
    const { id } = request.pathParameters;

    try {
      const post = await this.servicePost.get(id);

      if (!post) {
        return this.response(StatusCode.NOT_FOUND);
      }

      await this.servicePost.delete(id);

      return this.response(StatusCode.SUCCESS, {
        message: "resource deleted successfully",
      });
    } catch (error) {
      return this.response(StatusCode.ERROR, error);
    }
  }

  public async get(request: Request) {
    try {
      if (request.pathParameters !== null) {
        const { id } = request.pathParameters;
        const post = await this.servicePost.get(id);

        return this.response(
          !post ? StatusCode.NOT_FOUND : StatusCode.SUCCESS,
          post
        );
      }

      const posts = await this.servicePost.all();

      return this.response(StatusCode.SUCCESS, posts);
    } catch (error) {
      return this.response(500, error);
    }
  }
}
