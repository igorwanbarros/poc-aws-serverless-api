interface Post {
  id: string
  title: string
  description: string
  active: boolean
  created_at: string
  updated_at?: string
  deleted_at?: string
}

export default Post
