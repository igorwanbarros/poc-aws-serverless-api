import Post from "@domain/models/post";
import * as uuid from "uuid";
import { PostRepository } from "@infra/database";
import { inject, injectable } from "tsyringe";

@injectable()
export class PostService {
  constructor(
    @inject("PostRepository")
    private readonly repositoryPost: PostRepository
  ) {}

  public async create(
    partial: Pick<Post, "title" | "description">
  ): Promise<Post> {
    const id: string = uuid.v4();
    const post = await this.repositoryPost.create({
      ...partial,
      id: id,
      active: true,
      created_at: new Date().toISOString(),
      updated_at: null,
      deleted_at: null,
    });

    return post;
  }

  public async update(id: string, partial: Post): Promise<Post> {
    const post = await this.repositoryPost.update(id, {
      ...partial,
      id: id,
      updated_at: new Date().toISOString(),
    });

    return post;
  }

  public async get(id: string): Promise<Post | null> {
    const post = await this.repositoryPost.first(id);

    return post;
  }

  public async all(): Promise<Post[]> {
    const posts = await this.repositoryPost.all();

    return posts;
  }

  public async delete(id: string): Promise<boolean> {
    const deleted = await this.repositoryPost.delete(id);

    return deleted
  }
}
