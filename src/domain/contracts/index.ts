export enum StatusCode {
  SUCCESS = 200,
  CREATED = 201,
  NO_CONTENT = 204,
  NOT_FOUND = 404,
  VALIDATION_ERROR = 422,
  ERROR = 500,
}

interface Headers {
  [name: string]: string | undefined;
}

interface PathParameter {
  [name: string]: string | undefined;
}

interface QueryStringParameter {
  [name: string]: string | undefined;
}

export type Response = {
  statusCode: number;
  body: string;
};

export type Request = {
  headers: Headers;
  body: string | undefined | null | {[name: string]: any};
  pathParameters?: PathParameter;
  queryStringParameters?: QueryStringParameter;
};
