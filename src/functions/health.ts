import { StatusCode } from "@domain/contracts";
import { responseJson, middify } from "@infra/http";
import {
  APIGatewayEvent,
  APIGatewayProxyResult,
  Context,
  Handler,
} from "aws-lambda";

export const baseHandler = async (
  _event: APIGatewayEvent,
  _context: Context
): Promise<APIGatewayProxyResult> => {
  return responseJson(StatusCode.SUCCESS, { message: "OK" });
};

export const handler: Handler = middify(baseHandler);
