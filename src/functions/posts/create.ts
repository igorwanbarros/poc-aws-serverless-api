import {
  APIGatewayEvent,
  APIGatewayProxyResult,
  Context,
  Handler,
} from "aws-lambda";
import { middify } from "@infra/http";
import { bootstrap } from "@infra/dependency-injection";
import { PostController } from "@domain/controllers/post-controller";

const validation = {
  type: "object",
  required: ["body"],
  properties: {
    body: {
      required: ["title", "description"],
      type: "object",
      properties: {
        title: { type: "string" },
        description: { type: "string" },
      },
    },
  },
};

const baseHandler = async (
  event: APIGatewayEvent,
  _context: Context
): Promise<APIGatewayProxyResult> => {
  const controller: PostController = bootstrap.resolve('PostController')

  return await controller.create(event)
};

export const handler: Handler = middify(baseHandler, validation);
