import {
  APIGatewayEvent,
  APIGatewayProxyResult,
  Context,
  Handler,
} from "aws-lambda";
import { middify } from "@infra/http";
import { bootstrap } from "@infra/dependency-injection";
import { PostController } from "@domain/controllers/post-controller";

const baseHandler = async (
  event: APIGatewayEvent,
  _context: Context
): Promise<APIGatewayProxyResult> => {
  const controller: PostController = bootstrap.resolve('PostController')

  return await controller.delete(event)
};

export const handler: Handler = middify(baseHandler);
